import pygame
import random
import math
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation
import threading


# Initialize Pygame
pygame.init()

# Set up the display
width, height = 1200,800
screen = pygame.display.set_mode((width, height))
# Define the size of the miniature screen
miniature_width = width // 4  # For example, 1/4th of the main screen width
miniature_height = height // 4  # 1/4th of the main screen height

# Create the miniature screen surface
miniature_screen = pygame.Surface((miniature_width, miniature_height))

pygame.display.set_caption("Lunar Lander")

# Define colors
WHITE = (255, 255, 255)
RED = (255, 0, 0)
TRANSPARENT_SMOKY_BLURRY_WHITE = (255, 255, 255, 100)

class Lander:
    def __init__(self):
        self.reset()
        self.gun = Gun(self)


    def reset(self):
        self.x = width / 2
        self.y = 50
        self.vx = 0
        self.vy = 0
        self.angle = 0  # Angle in degrees
        self.fuel = 10000
        self.alive = True
        self.landed = False
        self.gravity = 0.03
        self.thrust_power = 0.25
        self.horizontal_thrust_power = 0.1
        self.rotation_speed = 2  # Speed of rotation

    def update(self):
        if not self.landed:
            # Apply gravity
            self.vy += self.gravity

        # Update position
        self.x += self.vx
        self.y += self.vy

        # Boundary check for lander
        self.x = max(0, min(self.x, width - 20))
        self.y = max(0, min(self.y, height - 40))

    def draw(self, surface):
         # Define the original triangle points
        point1 = (self.x, self.y - 20)  # Top point of the triangle
        point2 = (self.x - 10, self.y + 20)  # Bottom left
        point3 = (self.x + 10, self.y + 20)  # Bottom right

        # Get rotated points
        rotated_points = [self.rotate_point(p, self.angle) for p in [point1, point2, point3]]
        
        # Draw the triangle
        pygame.draw.polygon(surface, WHITE, rotated_points)

        self.draw_velocity_vector(surface)

    def thrust_up(self):
        if self.fuel > 0:
            radians = math.radians(self.angle)
            self.vx -= math.sin(radians) * self.thrust_power
            self.vy -= math.cos(radians) * self.thrust_power
            self.fuel -= 1
            self.create_particles()
    def thrust_down(self):
        if self.fuel > 0:
            self.vy += self.thrust_power
            self.fuel -= 1

    def thrust_left(self):
        if self.fuel > 0:
            self.vx -= self.horizontal_thrust_power
            self.fuel -= 1
            self.create_particles()

    def thrust_right(self):
        if self.fuel > 0:
            self.vx += self.horizontal_thrust_power
            self.fuel -= 1
            self.create_particles()
    
    def rotate_left(self):
        self.angle -= self.rotation_speed

    def rotate_right(self):
        self.angle += self.rotation_speed

    def rotate_point(self, point, angle):
        # Rotate a point around the lander's center
        radians = math.radians(angle)
        x, y = point
        cx, cy = self.x, self.y  # Center of rotation
        cos_angle = math.cos(radians)
        sin_angle = math.sin(radians)

        # Translate point to origin, rotate, and translate back
        new_x = cos_angle * (x - cx) - sin_angle * (y - cy) + cx
        new_y = sin_angle * (x - cx) + cos_angle * (y - cy) + cy

        return new_x, new_y
    
    def draw_velocity_vector(self, surface):
        # Scale factor for visualizing the velocity vector
        scale_factor = 5

        # Calculate the end point of the velocity vector
        end_x = self.x + self.vx * scale_factor
        end_y = self.y + self.vy * scale_factor

        # Draw the velocity vector line
        pygame.draw.line(surface,RED , (self.x, self.y), (end_x, end_y), 2)


    def create_particles(self):
        # Generate particles at the bottom of the lander
        for _ in range(5):  # Number of particles generated
            particles.append(Particle(self.x + 10, self.y + 40))

# class Particle:
#     def __init__(self, x, y):
#         self.x = x
#         self.y = y
#         self.size = random.randint(1, 2)
#         self.lifetime = random.randint(20, 50)
#         self.vx = random.uniform(-1, 1)
#         self.vy = random.uniform(-1, -3)

#     def update(self):
#         self.x -= self.vx
#         self.y -= self.vy
#         self.lifetime -= 1
#         self.vy -= 0.1  # gravity effect on particles

#     def draw(self, surface):
#         pygame.draw.circle(surface, WHITE, (int(self.x), int(self.y)), self.size)

#     def is_dead(self):
#         return self.lifetime <= 0
class Gun:
    def __init__(self, lander):
        self.lander = lander
        self.bullets = []

    def update(self):
        for bullet in self.bullets:
            bullet['x'] += bullet['vx']
            bullet['y'] += bullet['vy']
            # Optional: Remove bullets if they go off-screen
            if bullet['x'] < 0 or bullet['x'] > width or bullet['y'] < 0 or bullet['y'] > height:
                self.bullets.remove(bullet)

    def shoot_bullet(self, mouse_x, mouse_y):
        dx = mouse_x - self.lander.x
        dy = mouse_y - self.lander.y
        distance = math.sqrt(dx**2 + dy**2)
        speed = 5
        if distance != 0:
            vx = (dx / distance) * speed
            vy = (dy / distance) * speed
        else:
            vx = 0
            vy = 0
        bullet = {'x': self.lander.x, 'y': self.lander.y, 'vx': vx, 'vy': vy}
        self.bullets.append(bullet)
        print("Shooting bullet:", bullet)

    def draw(self, surface):
        for bullet in self.bullets:
            pygame.draw.circle(surface, WHITE, (int(bullet['x']), int(bullet['y'])), 5)



class Bullet:
    def __init__(self, x, y, mouse_x, mouse_y):
        self.x = x
        self.y = y
        self.mouse_x = mouse_x
        self.mouse_y = mouse_y
        self.speed = 5
        self.calculate_velocity()
    
    def calculate_velocity(self):
        dx = self.mouse_x - self.x
        dy = self.mouse_y - self.y
        distance = math.sqrt(dx**2 + dy**2)
        if distance != 0:
            self.vx = (dx / distance) * self.speed
            self.vy = (dy / distance) * self.speed
        else:
            self.vx = 0
            self.vy = 0

    def update(self,bullets):
        self.x += self.vx
        self.y += self.vy
        


class Particle:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.size = random.uniform(1, 4)
        self.lifetime = random.randint(20, 40)
        self.vx = random.uniform(-0.5, 0.5)
        self.vy = random.uniform(-1, -2)

    def update(self, particles):
        self.x -= self.vx
        self.y -= self.vy
        self.vy -= 0.05  # gravity effect on particles
        self.lifetime -= 1

        # Add fluid-like interaction
        for particle in particles:
            if particle != self:
                dx = particle.x - self.x
                dy = particle.y - self.y
                distance = dx * dx + dy * dy
                if distance < 20:
                    self.vx -= dx * 0.06
                    self.vy -= dy * 0.08
    def draw(self, surface):
        pygame.draw.circle(surface, TRANSPARENT_SMOKY_BLURRY_WHITE, (int(self.x), int(self.y)), int(self.size))
        self.draw_velocity_vector(surface)
    
    @property
    def is_dead(self):
        return self.lifetime <= 0
    
    def draw_velocity_vector(self, surface):
        # Scale factor for visualizing the velocity vector
        scale_factor = 5

        # Calculate the end point of the velocity vector
        end_x = self.x + self.vx * scale_factor
        end_y = self.y + self.vy * scale_factor

        # Draw the velocity vector line
        pygame.draw.line(surface,RED , (self.x, self.y), (end_x, end_y), 2)
    

global pygame_screen_data
# pygame_screen_data = None

# def capture_pygame(screen):
#     # Capture the Pygame surface and convert it to an array
#     screenshot = pygame.surfarray.array3d(screen)
#     screenshot = np.transpose(screenshot, (1, 0, 2))
#     return screenshot


def game_loop():

    # global pygame_screen_data


    global particles,bullets
    particles = []
    

    lander = Lander()
    gun = Gun(lander)
    clock = pygame.time.Clock()

    running = True
    
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONUP:
                mouse_x, mouse_y = pygame.mouse.get_pos()
                gun.shoot_bullet(mouse_x, mouse_y)
                print("Mouse clicked at:", mouse_x, mouse_y)  # Add this line for debugging

        # Check for key presses
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            lander.thrust_up()
        if keys[pygame.K_s]:
            lander.thrust_down()
        if keys[pygame.K_a]:
            lander.thrust_left()
        if keys[pygame.K_d]:
            lander.thrust_right()
        # Inside the game loop
        if keys[pygame.K_RIGHT]:
            lander.rotate_left()
        if keys[pygame.K_LEFT]:
            lander.rotate_right()

        if keys[pygame.K_r]:  # Press 'R' to reset the lander
            lander.reset()
        

        lander.update()
        gun.update()
        # Update particles
        for particle in particles:
            particle.update(particles)  # Pass the particles list here

        particles = [particle for particle in particles if not particle.is_dead]
        
        # # Check if lander has landed
        # if lander.y >= height - 40 and not lander.landed:
        #     lander.vy = 0
        #     lander.vx = 0
        #     lander.landed = True

        screen.fill((0, 0, 0))
        lander.draw(screen)
        gun.draw(screen)
        # Display fuel number at the top right of the screen
        fuel_text = pygame.font.SysFont(None, 24).render(f"Fuel: {lander.fuel}", True, WHITE)
        screen.blit(fuel_text, (width - fuel_text.get_width() - 10, 10))

        # Display fuel bar
        fuel_bar_width = lander.fuel * 2  # Adjust the width of the fuel bar based on the fuel value
        fuel_bar_height = 10
        fuel_bar_x = width - fuel_bar_width - 10
        fuel_bar_y = 40
        pygame.draw.rect(screen, WHITE, (fuel_bar_x, fuel_bar_y, fuel_bar_width, fuel_bar_height))
        # Display coordinates and velocities at the bottom left of the screen
        info_text = pygame.font.SysFont(None, 24).render(f"X: {int(lander.x)}, Y: {int(lander.y)}, VX: {int(lander.vx)}, VY: {int(lander.vy)}", True, WHITE)
        screen.blit(info_text, (10, height - info_text.get_height() - 10))
        
        for particle in particles:
            particle.draw(screen)

        pygame.transform.scale(screen, (miniature_width, miniature_height), miniature_screen)

        # Blit the miniature screen onto the top-right corner of the main screen
        screen.blit(miniature_screen, (width - miniature_width, height-miniature_height))

        # pygame_screen_data = capture_pygame(screen)
        
        pygame.display.flip()
        clock.tick(60)

    pygame.quit()

# Run the game loop in a separate thread
# game_thread = threading.Thread(target=game_loop)
# game_thread.start()

# # Set up Matplotlib plot
# fig, ax = plt.subplots()
# im = plt.imshow(np.zeros((800, 1200, 3)), animated=True)  # Initialize with a blank image
# plt.axis('off')

# def update_fig(*args):
#     if pygame_screen_data is not None:
#         im.set_array(pygame_screen_data)
#     return im,
# ani = FuncAnimation(fig, update_fig, interval=20, blit=True)
# plt.show()

# game_thread.join()
game_loop()